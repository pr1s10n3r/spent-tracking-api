package ginserver

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	tagservice "gitlab.com/algoru/spent-tracking-api/service/tag"
)

func RegisterTagRoutes(group *gin.RouterGroup, ts tagservice.Service) {
	tags := group.Group("/tags")
	tags.POST("", createTag(ts))
	tags.GET("", getTags(ts))
	tags.GET("/:id", getTag(ts))
	tags.PUT("/:id", editTag(ts))
	tags.DELETE("/:id", deleteTag(ts))
}

func createTag(ts tagservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		tag := tagservice.Tag{}

		if err := c.ShouldBindJSON(&tag); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid json body"})
			return
		}

		saved, err := ts.SaveTag(tag)
		if err != nil {
			statusCode, message := ts.GetTagErrorMessage(0, err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusCreated, saved)
	}
}

func getTags(ts tagservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		offset, err := strconv.ParseUint(c.Query("offset"), 10, 32)
		if err != nil {
			offset = 0
		}

		limit, err := strconv.ParseUint(c.Query("limit"), 10, 32)
		if err != nil {
			limit = 10
		}

		tags, err := ts.GetTags(uint(offset), uint(limit))
		if err != nil {
			statusCode, message := ts.GetTagErrorMessage(0, err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, tags)
	}
}

func getTag(ts tagservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		tagID, err := strconv.ParseUint(c.Param("id"), 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid tag id"})
			return
		}

		tag, err := ts.GetTag(uint(tagID))
		if err != nil {
			statusCode, message := ts.GetTagErrorMessage(uint(tagID), err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, tag)
	}
}

func editTag(ts tagservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		tagID, err := strconv.ParseUint(c.Param("id"), 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid tag id"})
			return
		}

		tag := tagservice.Tag{}
		if err := c.ShouldBindJSON(&tag); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid json body"})
			return
		}

		updatedTag, err := ts.EditTag(uint(tagID), tag)
		if err != nil {
			statusCode, message := ts.GetTagErrorMessage(uint(tagID), err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, updatedTag)
	}
}

func deleteTag(ts tagservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		tagID, err := strconv.ParseUint(c.Param("id"), 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid tag id"})
			return
		}

		tag, err := ts.DeleteTag(uint(tagID))
		if err != nil {
			statusCode, message := ts.GetTagErrorMessage(uint(tagID), err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, tag)
	}
}
