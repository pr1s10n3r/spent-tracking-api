package sqlite

import (
	"errors"
	"os"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var ErrInvalidDatabaseName = errors.New("invalid sqlite database name")

type Storage struct {
	db *gorm.DB
}

func NewStorage() (*Storage, error) {
	dbName := os.Getenv("SQLITE_DATABASE")
	if dbName == "" || !strings.Contains(dbName, ".db") {
		return nil, ErrInvalidDatabaseName
	}

	db, err := gorm.Open("sqlite3", dbName)
	if err != nil {
		return nil, err
	}

	storage := Storage{db: db}
	return &storage, nil
}
