package sqlite

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	tagrepository "gitlab.com/algoru/spent-tracking-api/repository/tag"
)

func (s *Storage) GetTagErrorMessage(tagID uint, err error) (int, string) {
	if err == gorm.ErrRecordNotFound {
		return http.StatusBadRequest, fmt.Sprintf("unable to find tag with id %d", tagID)
	}

	return http.StatusInternalServerError, err.Error()
}

func (s *Storage) GetTag(ID uint) (*tagrepository.Tag, error) {
	tag := tagrepository.Tag{ID: ID}

	if err := s.db.Find(&tag).Error; err != nil {
		return nil, err
	}

	return &tag, nil
}

func (s *Storage) GetTags(offset, limit uint) ([]tagrepository.Tag, error) {
	tags := make([]tagrepository.Tag, 0)

	if err := s.db.Offset(offset).Limit(limit).Find(&tags).Error; err != nil {
		return nil, err
	}

	return tags, nil
}

// TODO (@algoru): Implement this when transaction implementation is ready
func (s *Storage) GetMostProfitableTag() (*tagrepository.Tag, error) {
	return nil, nil
}

// TODO (@algoru): Implement this when transaction implementation is ready
func (s *Storage) GetLessProfitableTag() (*tagrepository.Tag, error) {
	return nil, nil
}

func (s *Storage) SaveTag(name string) (*tagrepository.Tag, error) {
	tag := tagrepository.Tag{Name: name}

	if err := s.db.Save(&tag).Error; err != nil {
		return nil, err
	}

	return &tag, nil
}

func (s *Storage) EditTag(ID uint, tag *tagrepository.Tag) (*tagrepository.Tag, error) {
	inStorageTag := tagrepository.Tag{ID: ID}

	if err := s.db.Find(&inStorageTag).Error; err != nil {
		return nil, err
	}

	inStorageTag.Name = tag.Name
	if err := s.db.Save(&inStorageTag).Error; err != nil {
		return nil, err
	}

	return &inStorageTag, nil
}

func (s *Storage) DeleteTag(ID uint) error {
	tag := tagrepository.Tag{ID: ID}
	return s.db.Delete(&tag).Error
}

func (s *Storage) TruncateTagsTable() error {
	_, err := s.db.Exec("DELETE FROM tag")
	return err
}
