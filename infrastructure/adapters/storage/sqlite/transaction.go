package sqlite

import (
	"fmt"
	"net/http"

	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	txrepository "gitlab.com/algoru/spent-tracking-api/repository/transaction"
)

func (s *Storage) GetTransactionErrorMessage(ID uint, err error) (int, string) {
	if err == gorm.ErrRecordNotFound {
		return http.StatusBadRequest, fmt.Sprintf("unable to find transaction with id %d", ID)
	}

	mysqlErr, ok := err.(*mysql.MySQLError)
	if ok {
		switch mysqlErr.Number {
		case 1452:
			return http.StatusBadRequest, fmt.Sprintf("transaction id %d doesn't exists", ID)
		}
	}

	return http.StatusInternalServerError, err.Error()
}

func (s *Storage) GetTransaction(ID uint) (*txrepository.Transaction, error) {
	transaction := txrepository.Transaction{ID: ID}

	if err := s.db.Find(&transaction).Related(&transaction.Tag).Error; err != nil {
		return nil, err
	}

	return &transaction, nil
}

func (s *Storage) GetTransactions(tf txrepository.TransactionFilter) ([]txrepository.Transaction, error) {
	transactions := make([]txrepository.Transaction, 0)

	query := s.db.Offset(tf.Offset).Limit(tf.Limit)
	if tf.From != nil {
		query = query.Where("created_at >= ?", tf.From)
	}
	if tf.To != nil {
		query = query.Where("created_at <= ?", tf.To)
	}
	if tf.TagID > 0 {
		query = query.Where("tag_id = ?", tf.TagID)
	}

	if err := query.Preload("Tag").Find(&transactions).Error; err != nil {
		return nil, err
	}

	return transactions, nil
}

func (s *Storage) SaveTransaction(tagID uint, value float64) (*txrepository.Transaction, error) {
	tx := txrepository.Transaction{
		TagID: tagID,
		Value: value,
	}

	if err := s.db.Save(&tx).Related(&tx.Tag).Error; err != nil {
		return nil, err
	}

	return &tx, nil
}

func (s *Storage) EditTransaction(ID uint, tx *txrepository.Transaction) (*txrepository.Transaction, error) {
	inStorageTx := txrepository.Transaction{ID: ID}

	if err := s.db.Find(&inStorageTx).Error; err != nil {
		return nil, err
	}

	inStorageTx.TagID = tx.TagID
	inStorageTx.Value = tx.Value

	if err := s.db.Save(&inStorageTx).Related(&inStorageTx.Tag).Error; err != nil {
		return nil, err
	}

	return &inStorageTx, nil
}

func (s *Storage) DeleteTransaction(ID uint) error {
	transaction := txrepository.Transaction{ID: ID}
	return s.db.Delete(&transaction).Error
}
