package mariadb

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Storage struct {
	db *gorm.DB
}

func NewStorage() (*Storage, error) {
	dataSource := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s?parseTime=true&charset=utf8",
		os.Getenv("MYSQL_USERNAME"),
		os.Getenv("MYSQL_PASSWORD"),
		os.Getenv("MYSQL_HOST"),
		os.Getenv("MYSQL_DATABASE"),
	)

	db, err := gorm.Open("mysql", dataSource)
	if err != nil {
		return nil, err
	}

	storage := Storage{db: db}
	return &storage, nil
}
