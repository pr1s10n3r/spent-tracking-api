package txservice

import "time"

type (
	Transaction struct {
		TagID uint    `json:"tag_id"`
		Value float64 `json:"value"`
	}

	TransactionFilter struct {
		Offset uint       `form:"offset,default=0"`
		Limit  uint       `form:"limit,default=10"`
		TagID  uint       `form:"tagId"`
		From   *time.Time `form:"from" time_format:"02-01-2006"`
		To     *time.Time `form:"to" time_format:"02-01-2006"`
	}
)
