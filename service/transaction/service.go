package txservice

import (
	"gitlab.com/algoru/spent-tracking-api/domain/entity"
	txrepository "gitlab.com/algoru/spent-tracking-api/repository/transaction"
)

type Service interface {
	GetTransactionErrorMessage(uint, error) (int, string)
	GetTransaction(uint) (*entity.Transaction, error)
	GetTransactions(TransactionFilter) ([]entity.Transaction, error)
	SaveTransaction(Transaction) (*entity.Transaction, error)
	EditTransaction(uint, Transaction) (*entity.Transaction, error)
	DeleteTransaction(uint) (*entity.Transaction, error)
}

type txService struct {
	repo txrepository.Repository
}

func NewService(r txrepository.Repository) Service {
	return &txService{repo: r}
}

func (s *txService) GetTransactionErrorMessage(txID uint, err error) (int, string) {
	return s.repo.GetTransactionErrorMessage(txID, err)
}

func (s *txService) GetTransaction(ID uint) (*entity.Transaction, error) {
	tx, err := s.repo.GetTransaction(ID)
	if err != nil {
		return nil, err
	}

	return &entity.Transaction{
		ID: tx.ID,
		Tag: entity.Tag{
			ID:        tx.Tag.ID,
			Name:      tx.Tag.Name,
			CreatedAt: tx.Tag.CreatedAt,
			UpdatedAt: tx.Tag.UpdatedAt,
			DeletedAt: tx.Tag.DeletedAt,
		},
		Value:     tx.Value,
		CreatedAt: tx.CreatedAt,
		UpdatedAt: tx.UpdatedAt,
		DeletedAt: tx.DeletedAt,
	}, nil
}

func (s *txService) GetTransactions(tf TransactionFilter) ([]entity.Transaction, error) {
	filter := txrepository.TransactionFilter{
		Offset: tf.Offset,
		Limit:  tf.Limit,
		TagID:  tf.TagID,
		From:   tf.From,
		To:     tf.To,
	}

	txs, err := s.repo.GetTransactions(filter)
	if err != nil {
		return nil, err
	}

	lenTransanctions := len(txs)
	transactions := make([]entity.Transaction, lenTransanctions)

	for i := 0; i < lenTransanctions; i++ {
		transactions[i].ID = txs[i].ID
		transactions[i].Tag = entity.Tag{
			ID:        txs[i].Tag.ID,
			Name:      txs[i].Tag.Name,
			CreatedAt: txs[i].Tag.CreatedAt,
			UpdatedAt: txs[i].Tag.UpdatedAt,
			DeletedAt: txs[i].Tag.DeletedAt,
		}
		transactions[i].Value = txs[i].Value
		transactions[i].CreatedAt = txs[i].CreatedAt
		transactions[i].UpdatedAt = txs[i].UpdatedAt
		transactions[i].DeletedAt = txs[i].DeletedAt
	}

	return transactions, nil
}

func (s *txService) SaveTransaction(tx Transaction) (*entity.Transaction, error) {
	savedTx, err := s.repo.SaveTransaction(tx.TagID, tx.Value)
	if err != nil {
		return nil, err
	}

	return &entity.Transaction{
		ID: savedTx.ID,
		Tag: entity.Tag{
			ID:        savedTx.Tag.ID,
			Name:      savedTx.Tag.Name,
			CreatedAt: savedTx.Tag.CreatedAt,
			UpdatedAt: savedTx.Tag.UpdatedAt,
			DeletedAt: savedTx.Tag.DeletedAt,
		},
		Value:     savedTx.Value,
		CreatedAt: savedTx.CreatedAt,
		UpdatedAt: savedTx.UpdatedAt,
		DeletedAt: savedTx.DeletedAt,
	}, nil
}

func (s *txService) EditTransaction(ID uint, tx Transaction) (*entity.Transaction, error) {
	repoTx := txrepository.Transaction{TagID: tx.TagID, Value: tx.Value}

	updatedTx, err := s.repo.EditTransaction(ID, &repoTx)
	if err != nil {
		return nil, err
	}

	return &entity.Transaction{
		ID: updatedTx.ID,
		Tag: entity.Tag{
			ID:        updatedTx.Tag.ID,
			Name:      updatedTx.Tag.Name,
			CreatedAt: updatedTx.Tag.CreatedAt,
			UpdatedAt: updatedTx.Tag.UpdatedAt,
			DeletedAt: updatedTx.Tag.DeletedAt,
		},
		Value:     updatedTx.Value,
		CreatedAt: updatedTx.CreatedAt,
		UpdatedAt: updatedTx.UpdatedAt,
		DeletedAt: updatedTx.DeletedAt,
	}, nil
}

func (s *txService) DeleteTransaction(ID uint) (*entity.Transaction, error) {
	tx, err := s.GetTransaction(ID)
	if err != nil {
		return nil, err
	}

	if err := s.repo.DeleteTransaction(ID); err != nil {
		return nil, err
	}

	return &entity.Transaction{
		ID: tx.ID,
		Tag: entity.Tag{
			ID:        tx.Tag.ID,
			Name:      tx.Tag.Name,
			CreatedAt: tx.Tag.CreatedAt,
			UpdatedAt: tx.Tag.UpdatedAt,
			DeletedAt: tx.Tag.DeletedAt,
		},
		Value:     tx.Value,
		CreatedAt: tx.CreatedAt,
		UpdatedAt: tx.UpdatedAt,
		DeletedAt: tx.DeletedAt,
	}, nil
}
