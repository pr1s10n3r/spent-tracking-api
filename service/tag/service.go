package tagservice

import (
	"errors"

	"gitlab.com/algoru/spent-tracking-api/domain/entity"
	tagrepository "gitlab.com/algoru/spent-tracking-api/repository/tag"
)

var (
	ErrRequiredTagName = errors.New("tag name is required")
)

type Service interface {
	GetTagErrorMessage(uint, error) (int, string)
	GetTag(uint) (*entity.Tag, error)
	GetTags(uint, uint) ([]entity.Tag, error)
	SaveTag(Tag) (*entity.Tag, error)
	EditTag(uint, Tag) (*entity.Tag, error)
	DeleteTag(uint) (*entity.Tag, error)
	RestoreTag(uint) error
}

type tagService struct {
	repo tagrepository.Repository
}

func NewService(r tagrepository.Repository) Service {
	return &tagService{repo: r}
}

func (s *tagService) GetTagErrorMessage(tagID uint, err error) (int, string) {
	return s.repo.GetTagErrorMessage(tagID, err)
}

func (s *tagService) GetTag(ID uint) (*entity.Tag, error) {
	tag, err := s.repo.GetTag(ID)
	if err != nil {
		return nil, err
	}

	return &entity.Tag{
		ID:        tag.ID,
		Name:      tag.Name,
		CreatedAt: tag.CreatedAt,
		UpdatedAt: tag.UpdatedAt,
		DeletedAt: tag.DeletedAt,
	}, nil
}

func (s *tagService) GetTags(offset, limit uint) ([]entity.Tag, error) {
	tags, err := s.repo.GetTags(offset, limit)
	if err != nil {
		return nil, err
	}

	lenTags := len(tags)
	entityTags := make([]entity.Tag, lenTags)

	for i := 0; i < lenTags; i++ {
		entityTags[i].ID = tags[i].ID
		entityTags[i].Name = tags[i].Name
		entityTags[i].CreatedAt = tags[i].CreatedAt
		entityTags[i].UpdatedAt = tags[i].UpdatedAt
		entityTags[i].DeletedAt = tags[i].DeletedAt
	}

	return entityTags, nil
}

func (s *tagService) SaveTag(tag Tag) (*entity.Tag, error) {
	tag.Sanitize()

	if tag.Name == "" {
		return nil, ErrRequiredTagName
	}

	savedTag, err := s.repo.SaveTag(tag.Name)
	if err != nil {
		return nil, err
	}

	return &entity.Tag{
		ID:        savedTag.ID,
		Name:      savedTag.Name,
		CreatedAt: savedTag.CreatedAt,
		UpdatedAt: savedTag.UpdatedAt,
		DeletedAt: savedTag.DeletedAt,
	}, nil
}

func (s *tagService) EditTag(ID uint, tag Tag) (*entity.Tag, error) {
	repoTag := tagrepository.Tag{Name: tag.Name}

	updatedTag, err := s.repo.EditTag(ID, &repoTag)
	if err != nil {
		return nil, err
	}

	return &entity.Tag{
		ID:        updatedTag.ID,
		Name:      updatedTag.Name,
		CreatedAt: updatedTag.CreatedAt,
		UpdatedAt: updatedTag.UpdatedAt,
		DeletedAt: updatedTag.DeletedAt,
	}, nil
}

func (s *tagService) DeleteTag(ID uint) (*entity.Tag, error) {
	tag, err := s.GetTag(ID)
	if err != nil {
		return nil, err
	}

	if err := s.repo.DeleteTag(ID); err != nil {
		return nil, err
	}

	return &entity.Tag{
		ID:        tag.ID,
		Name:      tag.Name,
		CreatedAt: tag.CreatedAt,
		UpdatedAt: tag.UpdatedAt,
		DeletedAt: tag.DeletedAt,
	}, nil
}

func (s *tagService) RestoreTag(ID uint) error {
	return s.repo.RestoreTag(ID)
}
