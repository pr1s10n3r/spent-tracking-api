package tagservice

import (
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/require"
	"gitlab.com/algoru/spent-tracking-api/domain/entity"
	"gitlab.com/algoru/spent-tracking-api/infrastructure/adapters/storage/mariadb"
)

func TestGetTag(t *testing.T) {
	testCases := []struct {
		name        string
		tagID       uint
		expected    *entity.Tag
		expectedErr error
	}{
		{
			name:  "get existing tag",
			tagID: 1,
			expected: &entity.Tag{
				ID:   1,
				Name: "changed name 1",
			},
			expectedErr: nil,
		},
		{
			name:        "get unexisting tag",
			tagID:       0,
			expected:    nil,
			expectedErr: gorm.ErrRecordNotFound,
		},
	}

	tagService := prepareTagService(t)

	for _, testCase := range testCases {
		fetchedTag, err := tagService.GetTag(testCase.tagID)
		if testCase.expected == nil {
			require.EqualError(t, err, testCase.expectedErr.Error())
		} else {
			require.NoError(t, err)
			require.Equal(t, testCase.expected.Name, fetchedTag.Name)
			require.NotNil(t, fetchedTag.CreatedAt)
			require.NotNil(t, fetchedTag.UpdatedAt)
		}
	}
}

func TestGetTags(t *testing.T) {
	testCases := []struct {
		name         string
		offset       uint
		limit        uint
		expectedTags int
	}{
		{
			name:         "get five tags",
			offset:       0,
			limit:        5,
			expectedTags: 5,
		},
		{
			name:         "get single tags",
			offset:       0,
			limit:        1,
			expectedTags: 1,
		},
		{
			name:         "get empty tags",
			offset:       0,
			limit:        0,
			expectedTags: 0,
		},
		{
			name:         "skip five and get five tags",
			offset:       5,
			limit:        5,
			expectedTags: 5,
		},
	}

	tagService := prepareTagService(t)

	for _, testCase := range testCases {
		fetchedTags, err := tagService.GetTags(testCase.offset, testCase.limit)
		require.NoError(t, err)

		if fetchedTags != nil {
			for _, fetchedTag := range fetchedTags {
				require.NotNil(t, fetchedTag.CreatedAt)
				require.NotNil(t, fetchedTag.UpdatedAt)
			}
		}

		require.Equal(t, testCase.expectedTags, len(fetchedTags))
	}
}

func TestSaveTag(t *testing.T) {
	testCases := []struct {
		name            string
		tagName         string
		expectedTagName string
		expectedErr     error
	}{
		{
			name:            "save tag with name",
			tagName:         "testing tag",
			expectedTagName: "testing tag",
			expectedErr:     nil,
		},
		{
			name:            "save tag with empty name",
			tagName:         "",
			expectedTagName: "",
			expectedErr:     ErrRequiredTagName,
		},
		{
			name:            "save tag name with html code",
			tagName:         "<h1>pwned tag</h1>",
			expectedTagName: "pwned tag",
			expectedErr:     nil,
		},
	}

	tagService := prepareTagService(t)

	for _, testCase := range testCases {
		savedTag, err := tagService.SaveTag(Tag{Name: testCase.tagName})

		if testCase.expectedErr == nil {
			require.NoError(t, err)
			require.Equal(t, testCase.expectedTagName, savedTag.Name)
			require.NotNil(t, savedTag.CreatedAt)
			require.NotNil(t, savedTag.UpdatedAt)
			require.Nil(t, savedTag.DeletedAt)
		} else {
			require.EqualError(t, err, testCase.expectedErr.Error())
		}
	}
}

func TestEdiTag(t *testing.T) {
	testCases := []struct {
		name         string
		tagID        uint
		editedName   string
		expectedName string
		expectedErr  error
	}{
		{
			name:         "change existing tag name",
			tagID:        1,
			editedName:   "changed name 1",
			expectedName: "changed name 1",
			expectedErr:  nil,
		},
		{
			name:         "change nonexistent tag name",
			tagID:        0,
			editedName:   "changed name 1",
			expectedName: "",
			expectedErr:  gorm.ErrRecordNotFound,
		},
	}

	tagService := prepareTagService(t)

	for _, testCase := range testCases {
		editedTag, err := tagService.EditTag(testCase.tagID, Tag{Name: testCase.editedName})

		if testCase.expectedErr == nil {
			require.NoError(t, err)
			require.Equal(t, testCase.tagID, editedTag.ID)
			require.Equal(t, testCase.expectedName, editedTag.Name)
			require.NotNil(t, editedTag.CreatedAt)
			require.NotNil(t, editedTag.UpdatedAt)
		} else {
			require.EqualError(t, err, testCase.expectedErr.Error())
		}
	}
}

func TestDeleteTag(t *testing.T) {
	testCases := []struct {
		name          string
		tagID         uint
		expectedTagID uint
		expectedErr   error
	}{
		{
			name:          "delete existing tag",
			tagID:         1,
			expectedTagID: 1,
			expectedErr:   nil,
		},
		{
			name:          "delete nonexistent tag",
			tagID:         0,
			expectedTagID: 0,
			expectedErr:   gorm.ErrRecordNotFound,
		},
	}

	tagService := prepareTagService(t)

	for _, testCase := range testCases {
		deletedTag, err := tagService.DeleteTag(testCase.tagID)

		if testCase.expectedErr == nil {
			require.NoError(t, err)
			require.Equal(t, testCase.expectedTagID, deletedTag.ID)
			require.NotNil(t, deletedTag.CreatedAt)
			require.NotNil(t, deletedTag.UpdatedAt)
		} else {
			require.EqualError(t, err, testCase.expectedErr.Error())
		}

		if deletedTag != nil {
			err = tagService.RestoreTag(deletedTag.ID)
			require.NoError(t, err)
		}
	}
}

func prepareTagService(t *testing.T) Service {
	err := godotenv.Load("../../.env")
	require.NoError(t, err)

	storage, err := mariadb.NewStorage()
	require.NoError(t, err)

	return NewService(storage)
}
