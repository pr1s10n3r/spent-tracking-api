# Spent Tracking API
This project is a production-ready example of how you should structure your hexagonal-architecture GoLang projects.

# Project Structure

```
spent-tracking-api        <------ This is your awesome project.
├───domain
│   └───entity            <------ Here is where your business inmutable entities lives
├───infrastructure        <------ Here is where all your external infrastructure implementation (web framework, database engine, etc) lives.
│   └───adapters          <------ Here you split in packages which kind of adapters (or implementation) are you writing.
│       ├───rest          <------ For example, REST implementations.
│       │   ├───ginserver <------ For example, a Gin-Gonic implementation.
│       │   └───port.go   <------ Here is where you define which methods your REST adapters must implements.
│       └───storage       <------ Here is where your storage implementations (or adapters) lives.
│           ├───mariadb   <------ For example, a MariaDB/MySQL adapter.
│           └───port.go   <------ Here is where you define which methods should your storage adapters must implement.
├───repository            <------ Here is where your business entities looks at a storage/database level.
│   ├───tag
│   └───transaction
└───service               <------ Here is where you define which services your application can serve. Not to be confused with web services.
    ├───tag
    └───transaction
```

# SQL Database Migration

I recommend [FlyWay](https://flywaydb.org/).

For MySQL/MariaDB:
```sh
$ flyway -url=jdbc:(mysql|mariadb)://<host>:<port>/<database> -user=<db user> -password=<user password> -locations=filesystem:./migrations/mariadb migrate
```

For SQLite:
```sh
$ flyway -url=jdbc:sqlite:<database>.db -user=<db user> -password=<user password> -locations=filesystem:./migrations/sqlite migrate
```

# TODO

- [ ] Testing.
- [ ] Jenkins CI/CD.

# Author

Álvaro Stagg [<alvarostagg@protonmail.com>](mailto:alvarostagg@protonmail.com)