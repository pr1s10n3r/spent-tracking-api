CREATE TABLE `tag` (
   `id`         INTEGER  NOT NULL PRIMARY KEY,
   `name`       TEXT     NOT NULL,
   `created_at` DATETIME NOT NULL,
   `updated_at` DATETIME NOT NULL,
   `deleted_at` DATETIME
);